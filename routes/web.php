<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->post('check_user', 'MainController@check_user_registration_status');
$router->post('register_user', 'MainController@register_user_and_token');

$router->get('create_token', 'MainController@create_token');



$router->get('test1', 'ExampleController@index');
$router->get('insert1', 'ExampleController@insertDummy');
$router->get('decrypt1', 'ExampleController@testDecrypt');
