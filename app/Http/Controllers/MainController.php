<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function check_user_registration_status(Request $request){
        $gid = $request->input('gid');

        //return "test";
        if(DB::table('player')->where('gid', $gid)->exists()){
            return 1;
        }else{
            return 0;
        }
        
    }

    public function register_user_and_token(Request $request){
         $gid = $request->input('gid');

         DB::table('player')->insert(['gid' => $gid, 'token' => "eouoeau"]);
    }

    public function create_token(Request $request){
         $gid = $request->input('gid');
         echo Crypt::encrypt($gid);
         
    }


}
