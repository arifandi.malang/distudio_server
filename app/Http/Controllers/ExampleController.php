<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Support\Facades\Crypt;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

      public function index(){

    	$result = DB::table('dummy_1')
    				->get();

    	echo Crypt::encrypt($result[0]->nomor_induk);

    }

    public function insertDummy(){

        DB::table('dummy_1')->insert(['nomor_induk' => 22222, 'isi' => "eouoeau"]);

    }

    public function testDecrypt(){
        echo Crypt::decrypt('eyJpdiI6ImlDZ1RaMXFNdWU0dkR6dndSSnozdWc9PSIsInZhbHVlIjoid1dWbkZZV0ZMWEdENHF4YnBVSEs2Zz09IiwibWFjIjoiNjQ4OWNkOGI4MTNjZDBjOWUyNmU4YjRmZjFhNTg2YjNkZTU4MjMzYThmZGFjMTU2MjJjMjNhYzNmNDkyM2UyMCJ9');
    }
    
}
